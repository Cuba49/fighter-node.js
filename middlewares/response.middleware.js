const responseMiddleware = (req, res, next) => {
    if (res.err) {
        console.log(res.err);
        res.status(400).json({ error: true, message: res.err.toString() });
    }else if (res.data) {
        res.status(200).json(res.data);
    }else {
        res.status(404).json({ error: true, message: "Request is not correct" });
    }

    // TODO: Implement middleware that returns result of the query
    next();
}

exports.responseMiddleware = responseMiddleware;