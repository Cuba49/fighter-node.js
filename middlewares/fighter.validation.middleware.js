const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if (Object.keys(fighter).some(key => !req.body[key] && key != 'id' && key != 'health')) {
        res.err = "Required fields are missing";
    }else if (Object.keys(req.body).some(key=>key=='id')) {
        res.err = "Id in the body of requests must be absent";
    }else if (Object.keys(req.body).some(key =>  fighter[key]==undefined )) {
        res.err = "Invalid fields are present";
    }else if (req.body.power&&(!Number.isInteger(req.body.power)||!(1<req.body.power&&req.body.power<100))) {
        res.err = "Power must be between 1 and 100";
    }else if (req.body.defense &&(!Number.isInteger(req.body.defense)||!(1<req.body.defense&&req.body.defense<10))) {
        res.err = "Defense must be between 1 and 10";
    }else if (req.body.health &&(!Number.isInteger(req.body.health)||!(80<req.body.health&&req.body.health<120))) {
        res.err = "Health must be between 80 and 120";
    }else if (req.body.name && FighterService.search(user => user.name == req.body.name)) {
        res.err = "Fighter with this name is already exists";
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    console.log(req.body);

    // TODO: Implement validatior for fighter entity during update
    if (!Object.keys(req.body).some(key => fighter[key]!=undefined && key != 'id')) {
        res.err = "You need at least one field from the model";
    }else if (Object.keys(req.body).some(key=>key=='id')) {
        res.err = "Id in the body of requests must be absent";
    }else if (Object.keys(req.body).some(key =>  fighter[key]==undefined )) {
        res.err = "Invalid fields are present";
    }else if (req.body.power&&(!Number.isInteger(req.body.power)||!(1<req.body.power&&req.body.power<100))) {
        res.err = "Power must be between 1 and 100";
    }else if (req.body.defense &&(!Number.isInteger(req.body.defense)||!(1<req.body.defense&&req.body.defense<10))) {
        res.err = "Defense must be between 1 and 10";
    }else if (req.body.health &&(!Number.isInteger(req.body.health)||!(80<req.body.health&&req.body.health<120))) {
        res.err = "Health must be between 80 and 120";
    }else if (req.body.name && FighterService.search(user => user.name == req.body.name)) {
        res.err = "Fighter with this name is already exists";
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;