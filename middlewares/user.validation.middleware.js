const { user } = require('../models/user');
const UserService = require('../services/userService');
const regGmail = /^([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com$/;
const regPhone = /^\+380([0-9]{9})$/;

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if (Object.keys(user).some(key =>  !req.body[key] && key != 'id')) {
        res.err = "Required fields are missing";
    }else if (Object.keys(req.body).some(key=>key=='id')) {
        res.err = "Id in the body of requests must be absent";
    }else if (Object.keys(req.body).some(key =>  user[key]==undefined )) {
        res.err = "Invalid fields are present";
    }else if (req.body.email&&!regGmail.test(req.body.email)) {
        res.err = "Mail must be correct and gmail";
    }else if (req.body.phoneNumber&&!regPhone.test(req.body.phoneNumber)) {
        res.err = "Phone number must be like +380xxxxxxxxx";
    }else if (req.body.password&&req.body.password.length<3) {
        res.err = "Password must be at least 3 characters";
    }else if (req.body.email && UserService.search(user => user.email == req.body.email)) {
        res.err = "User with this email is already exists";
    }else if (req.body.phoneNumber && UserService.search(user => user.phoneNumber == req.body.phoneNumber)) {
        res.err = "User with this phone number is already exists";
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if (!Object.keys(req.body).some(key => user[key]!=undefined&& key != 'id')) {
        res.err = "You need at least one field from the model";
    }else if (Object.keys(req.body).some(key=>key=='id')) {
        res.err = "Id in the body of requests must be absent";
    }else if (Object.keys(req.body).some(key =>  user[key]==undefined )) {
        res.err = "Invalid fields are present";
    }else if (req.body.email&&!regGmail.test(req.body.email)) {
        res.err = "Mail must be correct and gmail";
    }else if (req.body.phoneNumber&&!regPhone.test(req.body.phoneNumber)) {
        res.err = "Phone number must be like +380xxxxxxxxx";
    }else if (req.body.password&&req.body.password.length<3) {
        res.err = "Password must be at least 3 characters";
    }else if (req.body.email && UserService.search(user => user.email == req.body.email)) {
        res.err = "User with this email is already exists";
    }else if (req.body.phoneNumber && UserService.search(user => user.phoneNumber == req.body.phoneNumber)) {
        res.err = "User with this phone number is already exists";
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;